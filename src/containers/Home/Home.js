import React from 'react';
import { connect } from 'react-redux';
import { View, ScrollView, Text } from 'react-native';
import Swiper from 'react-native-swiper';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import PostsList from '../../components/PostsList';
import styles from './styles';
import {
  getPosts,
  joinPostChannel,
  leavePostChannel
} from '../../actions/postActions';
import {
  updateHomeUI
} from '../../actions/homeActions';

class Home extends React.Component {
  constructor(props) {
    super(props);

  }

  componentWillMount() {
    this.props.getPosts();
    this.props.joinPostChannel();
  }

  componentWillUnmount() {
    this.props.leavePostChannel();
  }

  filterPosts(posts, type) {
    return posts.filter(post => (
      post.type == type
    ));
  }

  handleScroll(updateHomeUI) {
    return (event) => {
      //console.log(event.nativeEvent.contentOffset.y);
      let pos = event.nativeEvent.contentOffset.y;
      updateHomeUI(pos);
    }
  }

  render() {
    console.log(this.props)
    return (
      <View style={styles.container}>
        {!this.props.isScroll.isScroll ?
          <View style={styles.container}>
            <Swiper
              height="100%"
              showsButtons={true}>
              <View></View>
              <View></View>
              <View></View>
            </Swiper>
          </View>
          : null
        }

        <ScrollableTabView
          style={styles.scrollableTabView}>
          <View style={styles.postsList} tabLabel="全部">
            <PostsList onScroll={this.handleScroll(this.props.updateHomeUI)}  posts={this.props.posts} navigator={this.props.navigator} />
          </View>
          <View style={styles.postsList} tabLabel="協尋">
            <PostsList posts={this.filterPosts(this.props.posts, 'lost')} navigator={this.props.navigator} />
          </View>
          <View style={styles.postsList} tabLabel="活動">
            <PostsList posts={this.filterPosts(this.props.posts, 'event')} navigator={this.props.navigator} />
          </View>
          <View style={styles.postsList} tabLabel="送養">
            <PostsList posts={this.filterPosts(this.props.posts, 'adopt')} navigator={this.props.navigator} />
          </View>
          <View style={styles.postsList} tabLabel="看護">
            <PostsList posts={this.filterPosts(this.props.posts, 'care')} navigator={this.props.navigator} />
          </View>
        </ScrollableTabView>
      </View>
    );
  }
}

export default connect((state) => ({
  posts: state.getIn(['post', 'posts']),
  isScroll: state.getIn(['ui','home']).toJS()
}), {
  getPosts,
  updateHomeUI,
  joinPostChannel,
  leavePostChannel
})(Home);
