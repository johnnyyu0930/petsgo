import { handleActions } from 'redux-actions';
import { UiState } from '../../constants/model';

const uiReducer = handleActions({
  UPDATE_HOME_UI: (state, { pos }) => {
    if(pos > 10) {
      return state.setIn(['home', 'isScroll'], true);
    }else {
      return state.setIn(['home', 'isScroll'], false);
    }
  }
}, UiState);

export default uiReducer;
