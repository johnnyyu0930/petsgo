import {
  UPDATE_HOME_UI
} from '../constants/actionTypes';

export const updateHomeUI = (pos) => (dispatch) => {
    dispatch({type: UPDATE_HOME_UI, pos});
};
