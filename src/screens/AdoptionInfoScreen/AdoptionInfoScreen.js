import React from 'react';
import AdoptionInfo from '../../containers/AdoptionInfo';

class AdoptionInfoScreen extends React.Component {
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  static navigatorButtons = {
    leftButtons: [
      {
        title:'取消',
        id: 'cancel'
      }
    ]
  };

  onNavigatorEvent(event) {
    if (event.type == 'NavBarButtonPress') {
      if (event.id == 'cancel') {
        this.props.navigator.dismissModal({
          screen:'petsgo.AdopetScreen',
          title:'首頁',
          passProps: {},
          animated:true,
          animationType: 'slide-down'
        });
      }
    }
  }

  render() {
    let { navigator, animal } = this.props;
    return (
      <AdoptionInfo navigator={navigator} animal={animal} />
    );
  }
}

export default AdoptionInfoScreen;
